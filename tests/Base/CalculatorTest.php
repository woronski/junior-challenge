<?php

namespace Bases;

class CalculatorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function shouldAddTwoNumbers()
    {
        // given
        $calculator = new \Calculator();

        // when
        $result = $calculator->add(1, 1);

        // then
        $this->assertEquals(2, $result);
    }

    /**
     * @test
     */
    public function shouldSubtractTwoNumbers()
    {
        // given
        $calculator = new \Calculator();

        // when
        $result = $calculator->subtract(3,1);

        // then
        $this->assertEquals(2, $result);
    }

    /**
     * @test
     */
	public function shouldMultiplyTwoNumbers() {
        // given
        $calculator = new \Calculator();

        // when
        $result = $calculator->multiply(2,8);

        // then
        $this->assertEquals(16, $result);
    }
    /**
     * @test
     */
	public function shouldDivideTwoNumbers() {
        // given
        $calculator = new \Calculator();

        // when
        $result = $calculator->divide(20,5);

        // then
        $this->assertEquals(4, $result);
    }
}
