Please implement all methods within `app/Calculator.php`. You have to implement the following mathematical operations:

- Add
- Subtract
- Multiply
- Divide

You can check the unit tests in `test/Base/CalculatorTest.php` for detailed requirements.
